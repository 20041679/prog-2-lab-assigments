#include <stdio.h>
#include "person.h"
#include "list.h"

int person_is_minor_adj(Person *person, Person *_) {
    (void)_;
    return person_is_minor(person);
}

int person_is_adult_adj(Person *person, Person *_) {
    (void)_;
    return !person_is_minor_adj(person, NULL);
}

void insert(List *list) {
    Person person;
    puts("inserire persona");
    person_input(&person);

    int (*predicate) (Person *, Person *) = person_is_minor(&person)
        ? person_is_minor_adj
        : person_is_adult_adj;
    List *found;
    if ((found = list_find_ref(list, predicate, NULL))) {
        List next = *found;
        *found = node_alloc();
        (*found)->data = person;
        (*found)->next = next;
    } else {
        list_insert_head(list, person);
    }
}

List filter_relatives(List list, Person *relative) {
    return list_filter(list, person_is_relative, relative);
}

int main(void) {
    List list = NULL;

    Person tmp;
    puts("persona da inserire in fondo");
    person_input(&tmp);
    list_insert_tail(&list, tmp);

    list_output(list, person_output);
    
    puts("persona da inserire in testa");
    person_input(&tmp);
    list_insert_head(&list, tmp);

    insert(&list);

    list_output(list, person_output);

    puts("persona parente");
    person_input(&tmp);
    List relatives = filter_relatives(list, &tmp);
    puts("parenti");
    list_output(relatives, person_output);
}
