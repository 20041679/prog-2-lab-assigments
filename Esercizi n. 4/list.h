#ifndef LIST_H
#define LIST_H

#include "person.h"

typedef Person Data;

typedef struct Head {
    Person data;
    struct Head *next;
} Head;

typedef Head *List;

List node_alloc(void);
void list_insert_tail(List *list, Data data);
void list_insert_head(List *list, Data data);
List list_filter(List list, int (*predicate) (Data *, Data *), Data *key);
List *list_find_ref(List *list, int (*predicate) (Data *, Data *), Data *key);
void list_output(List list, void (*print) (Data *));

#endif
