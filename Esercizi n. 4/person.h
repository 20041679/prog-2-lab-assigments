#ifndef PERSON_H
#define PERSON_H

typedef struct {
    char first_name[64];
    char last_name[64];
    unsigned int age;
} Person;

void person_input(Person *person);
int person_is_relative(Person *p1, Person *p2);
void person_output(Person *person);
int person_is_minor(Person *person);

#endif
