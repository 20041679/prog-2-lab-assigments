#include <stdlib.h>
#include "list.h"

List node_alloc(void) {
    return malloc(sizeof(Head));
}

void list_insert_tail(List *list, Data data) {
    while (*list) {
        list = &(*list)->next;
    }
    *list = node_alloc();
    (*list)->data = data;
    (*list)->next = NULL;
}

void list_insert_head(List *list, Data data) {
    List next = *list;
    *list = node_alloc();
    (*list)->data = data;
    (*list)->next = next;
}

List *list_find_ref(List *list, int (*predicate) (Data *, Data *), Data *key) {
    if (*list) {
        return predicate(&(*list)->data, key)
            ? list
            : list_find_ref(&(*list)->next, predicate, key);
    }
    return NULL;
}

List list_filter(List list, int (*predicate) (Data *, Data *), Data *key) {
    if (list) {
        if (predicate(&list->data, key)) {
            List new = node_alloc();
            new->data = list->data;
            new->next = list_filter(list->next, predicate, key);
            return new;
        }
        return list_filter(list->next, predicate, key);
    }
    return NULL;
}

void list_output(List list, void (*print) (Data *)) {
    if (list) {
        print(&list->data);
        list_output(list->next, print);
    }
}
