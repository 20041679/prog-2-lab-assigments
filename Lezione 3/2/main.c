#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ARR_LEN(arr) (sizeof(arr) / sizeof(*arr))

typedef struct {
    char first_name[64];
    char last_name[64];
    unsigned int age;
} Person;

int person_parse(char *s, Person *person) {
    char format[4 + 1 + 4 + 1 + 2 + 1];
    assert((size_t)snprintf(
        format,
        ARR_LEN(format),
        "%%%zus %%%zus %%u",
        ARR_LEN(person->first_name),
        ARR_LEN(person->last_name)
    ) < ARR_LEN(format));

    return sscanf(
        s,
        format,
        person->first_name,
        person->last_name,
        &person->age
    ) < 3 ? -1 : 0;
}

int person_read(FILE *f, Person *person) {
    char buf[256];
    fgets_strip(buf, ARR_LEN(buf), f);

    if (strncmp(buf, FILE_END, ARR_LEN(FILE_END)) == 0) {
        return 0;
    }
    return person_parse(buf, person) < 0 ? 0 : 1;
}

size_t people_read_n(FILE *f, Person people[], size_t n) {
    for (size_t i = 0; i < n; ++i) {
        if (!person_read(f, people[i])) {
            return i;
        }
    }
    return n;
}

void people_print_n(Person people[], size_t n) {
    for (size_t i = 0; i < n; ++i) {
        Person p = people[i];
        printf(
            "Nome: %s, Cognome: %s, Anni: %u\n",
            p[i].first_name, p[i].last_name, p[i].age
        );
    }
}

int main(void) {
    Person people[3];
    
    FILE *f;
    if (!(f = fopen("input.txt"))) {
        return 1;
    }
    size_t people_read = people_read_n(f, people, ARR_LEN(people));
    fclose(f);

    people_print_n(people, people_read);
}
