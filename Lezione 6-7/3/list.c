#include "list.h"
#include <stdlib.h>

Link list_new_raw(void) {
    return malloc(sizeof(ListHead));
}

Link list_new(Data data[], size_t n) {
    if (n == 0) {
        return NULL;
    } else {
        Link list = list_new_raw();
        list->data = data[0];
        list->next = list_new(&data[1], n - 1);
        return list;
    }
}

Link list_input(FILE *f, int (*input) (FILE *, Data *)) {
    Data data;

    if (input(f, &data)) {
        Link head = list_new(&data, 1);
        head->next = list_input(f, input);
        return head;
    }

    return NULL;
}

void list_print(Link head, void (*print) (Data *)) {
    if (head) {
        print(&head->data);
        list_print(head->next, print);
    }
}

Link list_nth(Link head, size_t n) {
    if (head) {
        return n > 0 ? list_nth(head->next, n - 1) : head;
    }
    return NULL;
}

Link list_find(Link head, int (*compare) (Data *, Data *), Data *key) {
    if (head) {
        if (compare(&head->data, key)) {
            return head;
        }
        return list_find(head->next, compare, key);
    }
    return NULL;
}


void _list_insert_after(Link before, Data *data) {
    Link new = list_new(data, 1);
    new->next = before->next;
    before->next = new;
}

// returns the link of the newly inserted node, NULL if insertion failed
Link list_insert_after(Link head, int (*compare) (Data *, Data *), Data *key) {
    Link found = list_find(head, compare, key);
    if (found) {
        _list_insert_after(found, key);
        return found->next;
    }
    return NULL;
}
