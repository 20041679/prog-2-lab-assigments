#ifndef LIST_H
#define LIST_H

#include "person.h"
#include <stdio.h>

typedef Person Data;

typedef struct ListHead {
    struct ListHead *next;
    Data data;
} ListHead;

typedef ListHead *Link;

Link list_new(Data data[], size_t n);
Link list_input(FILE *f, int (*input) (FILE *, Data *));
void list_print(Link head, void (*print) (Data *));
Link list_find(Link head, int (*compare) (Data *, Data *), Data *key);
Link list_nth(Link head, size_t n);
Link list_insert_after(Link head, int (*compare) (Data *, Data *), Data *key);

#endif
