#include "list.h"
#include <stdlib.h>

Link list_new_raw(void) {
    return malloc(sizeof(ListHead));
}

Link list_new(Data data) {
    Link ret = list_new_raw();
    ret->data = data;
    ret->next = NULL;
    return ret;
}

Link list_input(FILE *f, int (*input) (FILE *, Data *)) {
    Data data;

    if (input(f, &data)) {
        Link head = list_new(data);
        head->next = list_input(f, input);
        return head;
    }

    return NULL;
}

void list_print(Link head, void (*print) (Data *)) {
    if (head) {
        print(&head->data);
        list_print(head->next, print);
    }
}
