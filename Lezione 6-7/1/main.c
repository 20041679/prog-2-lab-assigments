#include <stdio.h>
#include <stdlib.h>
#include "list.h"
#include "person.h"

int main(int argc, char *argv[]) {
    if (argc != 2) {
        return EXIT_FAILURE;
    }
    FILE *f;
    if (!(f = fopen(argv[1], "r"))) {
        return EXIT_FAILURE;
    }

    Link head = list_input(f, person_read);
    fclose(f);

    list_print(head, person_print);
}
