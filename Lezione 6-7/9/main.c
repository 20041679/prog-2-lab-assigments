#include <stdio.h>
#include <stdlib.h>
#include "list.h"
#include "person.h"
#include "input.h"
#include "util.h"

int main(int argc, char *argv[]) {
    if (argc != 3) {
        return EXIT_FAILURE;
    }
    FILE *f;
    if (!(f = fopen(argv[1], "r"))) {
        return EXIT_FAILURE;
    }

    Link l1 = list_input(f, person_read);
    fclose(f);

    if (!(f = fopen(argv[2], "r"))) {
        return EXIT_FAILURE;
    }
    Link l2 = list_input(f, person_read);
    fclose(f);

    puts("lista 1:");
    list_print(l1, person_print);
    puts("");
    puts("lista 2:");
    list_print(l2, person_print);
    puts("");

    int compare = list_compare(l1, l2, person_compare);
    printf("la lista 1 è %s rispetto alla lista 2\n",
        compare == 0
            ? "uguale"
            : compare < 0
                ? "minore"
                : "maggiore");
}
