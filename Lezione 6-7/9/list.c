#include "list.h"
#include "person.h"
#include <stdlib.h>

Link list_new_raw(void) {
    return malloc(sizeof(ListHead));
}

Link list_new(Data data[], size_t n) {
    if (n == 0) {
        return NULL;
    } else {
        Link list = list_new_raw();
        list->data = data[0];
        list->next = list_new(&data[1], n - 1);
        return list;
    }
}

Link list_input(FILE *f, int (*input) (FILE *, Data *)) {
    Data data;

    if (input(f, &data)) {
        Link head = list_new(&data, 1);
        head->next = list_input(f, input);
        return head;
    }

    return NULL;
}

void list_print(Link head, void (*print) (Data *)) {
    if (head) {
        print(&head->data);
        list_print(head->next, print);
    }
}

int list_compare(Link l1, Link l2, int (*compare) (Data *, Data *)) {
    if (l1 && l2) {
        int ret = compare(&l1->data, &l2->data);
        return ret == 0
            ? list_compare(l1->next, l2->next, compare)
            : ret;
    } else if (l1) {
        return 1;
    } else if (l2) {
        return -1;
    } else {
        return 0;
    }
}
