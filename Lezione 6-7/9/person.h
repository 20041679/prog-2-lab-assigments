#ifndef PERSON_H
#define PERSON_H

#include "input.h"
#include <stdio.h>

#define FILE_END "END"

typedef struct {
    char first_name[64];
    char last_name[64];
    unsigned int age;
} Person;


int person_parse(char *s, Person *person);
int person_read(FILE *f, Person *person);
Person person_input();
void person_print(Person *person);
int person_compare_name(Person *p1, Person *p2);
int person_compare_age(Person *p1, Person *p2);
int person_compare(Person *p1, Person *p2);

#endif
