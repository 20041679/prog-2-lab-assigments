#ifndef LIST_H
#define LIST_H

#include "person.h"
#include <stdio.h>

typedef Person Data;

typedef struct ListHead {
    struct ListHead *next;
    Data data;
} ListHead;

typedef ListHead *Link;

Link list_new(Data data[], size_t n);
Link list_input(FILE *f, int (*input) (FILE *, Data *));
void list_print(Link head, void (*print) (Data *));
// returns 0 if the lists are equal, -1 if l1 < l2, +1 if l1 > l2
int list_compare(Link l1, Link l2, int (*compare) (Data *, Data *));

#endif
