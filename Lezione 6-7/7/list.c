#include "list.h"
#include <stdlib.h>

Link list_new_raw(void) {
    return malloc(sizeof(ListHead));
}

Link list_new(Data data[], size_t n) {
    if (n == 0) {
        return NULL;
    } else {
        Link list = list_new_raw();
        list->data = data[0];
        list->next = list_new(&data[1], n - 1);
        return list;
    }
}

Link list_input(FILE *f, int (*input) (FILE *, Data *)) {
    Data data;

    if (input(f, &data)) {
        Link head = list_new(&data, 1);
        head->next = list_input(f, input);
        return head;
    }

    return NULL;
}

void list_print(Link head, void (*print) (Data *)) {
    if (head) {
        print(&head->data);
        list_print(head->next, print);
    }
}

Link _list_reverse(Link head, Link acc) {
    if (head) {
        Link new = list_new(&head->data, 1);
        new->next = acc;
        return _list_reverse(head->next, new);
    }
    return acc;
}

Link list_reverse(Link head) {
    return _list_reverse(head, NULL);
}
