#include <stdio.h>
#include <stdlib.h>
#include "list.h"
#include "person.h"
#include "input.h"
#include "util.h"

#define MESSAGE_NO_RELATIVES "non ha parenti"

int main(int argc, char *argv[]) {
    if (argc != 2) {
        return EXIT_FAILURE;
    }
    FILE *f;
    if (!(f = fopen(argv[1], "r"))) {
        return EXIT_FAILURE;
    }

    Link list = list_input(f, person_read);
    fclose(f);

    puts("lista:");
    list_print(list, person_print);
    puts("");
    puts("lista invertita:");
    list_print(list_reverse(list), person_print);
}
