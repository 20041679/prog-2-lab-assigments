#include "input.h"
#include <stdio.h>
#include <string.h>

char *fgets_strip(char *s, int size, FILE *stream) {
    char *ret = fgets(s, size, stream);
    s[strcspn(s, "\n")] = 0;
    return ret;
}

void flush_stdin(void) {
    int c;
    while ((c = getchar()) != '\n' && c != EOF);
}
