#include <stdio.h>
#include <stdlib.h>
#include "list.h"
#include "person.h"
#include "input.h"
#include "util.h"

#define MESSAGE_NOT_FOUND "persona non trovata"

int main(int argc, char *argv[]) {
    if (argc != 2) {
        return EXIT_FAILURE;
    }
    FILE *f;
    if (!(f = fopen(argv[1], "r"))) {
        return EXIT_FAILURE;
    }

    Link list = list_input(f, person_read);
    fclose(f);

    puts("ricerca per [c]ognome o [p]osizione?");
    int choice = getchar();
    flush_stdin();

    Link found;
    switch (choice) {
        case 'c': {
            puts("inserire cognome");
            Person key;
            fgets_strip(key.last_name, ARR_LEN(key.last_name), stdin);
            found = list_find(list, person_is_related, &key);
            break;
        }
        case 'p': {
            puts("inserire posizione");
            size_t n;
            scanf("%zu", &n);
            found = list_nth(list, n);
            break;
        }
        default:
            return EXIT_FAILURE;
            break;
    }

    if (found) {
        person_print(&found->data);
    } else {
        fputs(MESSAGE_NOT_FOUND "\n", stderr);
    }
}
