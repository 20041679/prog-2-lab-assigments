#include <stdio.h>
#include <stdlib.h>
#include "list.h"
#include "person.h"
#include "input.h"
#include "util.h"

#define MESSAGE_NO_RELATIVES "non ha parenti"

int main(int argc, char *argv[]) {
    if (argc != 2) {
        return EXIT_FAILURE;
    }
    FILE *f;
    if (!(f = fopen(argv[1], "r"))) {
        return EXIT_FAILURE;
    }

    Link list = list_input(f, person_read);
    fclose(f);

    puts("inserimento nuova persona");
    Person insert = person_input();
    if (list_insert_after_all(list, person_is_related, &insert)) {
        list_print(list, person_print);
    } else {
        puts(MESSAGE_NO_RELATIVES);
    }
}
