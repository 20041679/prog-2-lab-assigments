#ifndef LIST_H
#define LIST_H

#include "person.h"
#include <stdio.h>

typedef Person Data;

typedef struct ListHead {
    struct ListHead *next;
    Data data;
} ListHead;

typedef ListHead *Link;

Link list_new(Data data[], size_t n);
Link list_input(FILE *f, int (*input) (FILE *, Data *));
void list_print(Link head, void (*print) (Data *));
Link list_find(Link head, int (*predicate) (Data *));
Link list_find_previous(Link head, int (*predicate) (Data *));
Link list_nth(Link head, size_t n);
size_t list_length(Link head);
Link list_filter(Link head, int (*filter) (Data *));
size_t list_filter_count(Link head, int (*filter) (Data *));
Link list_del(Link head, int (*predicate) (Data *));
size_t list_del_all(Link head, int (*predicate) (Data *));

#endif
