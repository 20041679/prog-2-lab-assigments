#include "list.h"
#include <stdlib.h>

Link list_new_raw(void) {
    return malloc(sizeof(ListHead));
}

Link list_new(Data data[], size_t n) {
    if (n == 0) {
        return NULL;
    } else {
        Link list = list_new_raw();
        list->data = data[0];
        list->next = list_new(&data[1], n - 1);
        return list;
    }
}

Link list_input(FILE *f, int (*input) (FILE *, Data *)) {
    Data data;

    if (input(f, &data)) {
        Link head = list_new(&data, 1);
        head->next = list_input(f, input);
        return head;
    }

    return NULL;
}

void list_print(Link head, void (*print) (Data *)) {
    if (head) {
        print(&head->data);
        list_print(head->next, print);
    }
}

Link list_nth(Link head, size_t n) {
    if (head) {
        return n > 0 ? list_nth(head->next, n - 1) : head;
    }
    return NULL;
}

Link list_find(Link head, int (*predicate) (Data *)) {
    if (head) {
        if (predicate(&head->data)) {
            return head;
        }
        return list_find(head->next, predicate);
    }
    return NULL;
}

Link list_find_previous(Link head, int (*predicate) (Data *)) {
    if (head) {
        if (head->next) {
            if (predicate(&head->next->data)) {
                return head;
            }
        }
    }
    return NULL;
}

size_t list_length(Link head) {
    return head ? 1 + list_length(head->next) : 0;
}

Link list_filter(Link head, int (*filter) (Data *)) {
    if (head) {
        if (filter(&head->data)) {
            Link new = list_new(&head->data, 1);
            new->next = list_filter(head->next, filter);
            return new;
        } else {
            return list_filter(head->next, filter);
        }
    }
    return NULL;
}

size_t list_filter_count(Link head, int (*filter) (Data *)) {
    // could be implemented more efficiently, without
    // reusing `list_filter`
    return list_length(list_filter(head, filter));
}

// deletes the first node that for which `predicate` is true
// returns the node before the deleted one
Link list_del(Link head, int (*predicate) (Data *)) {
    Link previous;
    if ((previous = list_find_previous(head, predicate))) {
        Link to_delete = previous->next;
        previous->next = to_delete->next;
        free(to_delete);
        return previous;
    }
    return NULL;
}

size_t list_del_all(Link head, int (*predicate) (Data *)) {
    size_t counter = 0;
    while ((head = list_del(head, predicate))) {
        ++counter;
    }
    return counter;
}
