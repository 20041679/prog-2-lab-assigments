#include <stdio.h>
#include <stdlib.h>
#include "list.h"
#include "person.h"
#include "input.h"
#include "util.h"

#define MESSAGE_NO_RELATIVES "non ha parenti"

int main(int argc, char *argv[]) {
    if (argc != 2) {
        return EXIT_FAILURE;
    }
    FILE *f;
    if (!(f = fopen(argv[1], "r"))) {
        return EXIT_FAILURE;
    }

    Link list = list_input(f, person_read);
    fclose(f);

    size_t n_adults = list_filter_count(list, person_is_adult);
    printf("ci sono %zu adulti\n", n_adults);
    Link adults = list_filter(list, person_is_adult);
    if (adults) {
        puts("lista adulti:");
        list_print(adults, person_print);
        puts("");
    }

    list_del_all(list, person_is_adult);
    if (list) {
        puts("lista minorenni:");
        list_print(list, person_print);
    }
}
