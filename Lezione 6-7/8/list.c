#include "list.h"
#include "person.h"
#include <stdlib.h>

Link list_new_raw(void) {
    return malloc(sizeof(ListHead));
}

Link list_new(Data data[], size_t n) {
    if (n == 0) {
        return NULL;
    } else {
        Link list = list_new_raw();
        list->data = data[0];
        list->next = list_new(&data[1], n - 1);
        return list;
    }
}

Link list_input(FILE *f, int (*input) (FILE *, Data *)) {
    Data data;

    if (input(f, &data)) {
        Link head = list_new(&data, 1);
        head->next = list_input(f, input);
        return head;
    }

    return NULL;
}

void list_print(Link head, void (*print) (Data *)) {
    if (head) {
        print(&head->data);
        list_print(head->next, print);
    }
}

Link list_merge_sorted(Link l1, Link l2, int (*compare) (Data *, Data *)) {
    if (!l1) { return l2; }
    if (!l2) { return l1; }

    if (compare(&l1->data, &l2->data) < 0) {
        l1->next = list_merge_sorted(l1->next, l2, compare);
        return l1;
    } else {
        l2->next = list_merge_sorted(l1, l2->next, compare);
        return l2;
    }
}
