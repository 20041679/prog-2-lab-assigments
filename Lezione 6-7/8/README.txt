Non si capisce se li voglia ordinati in base all'età o in ordine lessicografico.
Questa implementazione li ordina in base all'età ma basta cambiare la funzione
passata al parametro `compare` della funzione `list_merge_sorted`.

Esempio per ordinarli per cognome-nome:

int cmp(Person *p1, Person *p2) {
    int ret = strcmp(p1->last_name, p2->last_name);
    if (ret == 0) {
        return strcmp(p1->first_name, p2->first_name);
    }
    return ret;
}

List res = list_merge_sorted(l1, l2, cmp);
