#include "person.h"
#include "util.h"
#include <stdio.h>
#include <string.h>
#include <assert.h>

int person_parse(char *s, Person *person) {
    char format[4 + 1 + 4 + 1 + 2 + 1];
    assert((size_t)snprintf(
        format,
        ARR_LEN(format),
        "%%%zus %%%zus %%u",
        ARR_LEN(person->first_name),
        ARR_LEN(person->last_name)
    ) < ARR_LEN(format));

    return sscanf(
        s,
        format,
        person->first_name,
        person->last_name,
        &person->age
    ) < 3 ? -1 : 0;
}

int person_read(FILE *f, Person *person) {
    char buf[256];
    fgets_strip(buf, ARR_LEN(buf), f);

    if (strncmp(buf, FILE_END, ARR_LEN(FILE_END)) == 0) {
        return 0;
    }
    return person_parse(buf, person) < 0 ? 0 : 1;
}

Person person_input() {
    Person person;
    puts("nome");
    fgets_strip(person.first_name, ARR_LEN(person.first_name), stdin);
    puts("cognome");
    fgets_strip(person.last_name, ARR_LEN(person.last_name), stdin);
    puts("anni");
    scanf("%u", &person.age);
    return person;
}

void person_print(Person *person) {
    printf(
        "nome: %s, cognome: %s, eta': %u\n",
        person->first_name,
        person->last_name,
        person->age
    );
}

int person_is_related(Person *p1, Person *p2) {
    return strcmp(p1->last_name, p2->last_name) == 0;
}

int person_is_adult(Person *p) {
    return p->age >= 18;
}

int person_younger(Person *p1, Person *p2) {
    if (p1->age < p2->age) { return -1; }
    else if (p1->age == p2->age) { return 0; }
    else { return 1; }
}
