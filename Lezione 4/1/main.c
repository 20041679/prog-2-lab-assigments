#include <stdio.h>
#include <stdlib.h>

typedef int Data;

typedef struct ListHead {
    struct ListHead *next;
    Data data;
} ListHead;

typedef ListHead *LINK;

LINK list_new_raw(void) {
    return malloc(sizeof(ListHead));
}

LINK list_new(Data data) {
    LINK ret = list_new_raw();
    ret->data = data;
    ret->next = NULL;
    return ret;
}

LINK list_input(void) {
    ListHead *head = NULL;
    ListHead **next = &head;

    puts("prossimo elemento");
    int input;
    scanf("%d", &input);
    while (input > 0) {
        *next = list_new(input);
        puts("prossimo elemento");
        scanf("%d", &input);
        next = &(*next)->next;
    }
    return head;
}

void list_print(LINK head) {
    while (head) {
        printf("%d -> ", head->data);
        head = head->next;
    }
    puts("");
}

int main(void) {
    list_print(list_input());
}
