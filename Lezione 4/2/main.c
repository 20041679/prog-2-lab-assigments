#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ARR_LEN(arr) (sizeof(arr) / sizeof(*arr))

typedef struct {
    char first_name[64];
    char last_name[64];
    unsigned int age;
} Person;

void stdin_flush(void) {
    int c;
    while ((c = getchar()) != '\n' && c != EOF);
}

void str_input(char *s, size_t size) {
    fgets(s, size, stdin);
    s[strcspn(s, "\n")] = '\0';
}

int person_read(Person *person) {
    puts("nome");
    str_input(person->first_name, ARR_LEN(person->first_name));
    if (strcmp(person->first_name, "end") == 0) {
        return 0;
    }
    puts("cognome");
    str_input(person->last_name, ARR_LEN(person->last_name));
    puts("eta'");
    scanf("%u", &person->age);

    // get rid of newline
    getchar();

    return 1;
}

void person_print(Person *person) {
    printf(
        "nome: %s, cognome: %s, eta': %u\n",
        person->first_name,
        person->last_name,
        person->age
    );
}

typedef Person Data;

typedef struct ListHead {
    struct ListHead *next;
    Data data;
} ListHead;

typedef ListHead *Link;

Link list_new_raw(void) {
    return malloc(sizeof(ListHead));
}

Link list_new(Data data) {
    Link ret = list_new_raw();
    ret->data = data;
    ret->next = NULL;
    return ret;
}

Link list_input(int (*input) (Data *)) {
    ListHead *head = NULL;
    ListHead **next = &head;

    Data data;
    while (
        puts("prossimo elemento"),
        input(&data)
    ) {
        *next = list_new(data);
        next = &(*next)->next;
    }
    return head;
}

void list_print(Link head, void (*print) (Data *)) {
    while (head) {
        print(&head->data);
        head = head->next;
    }
    puts("");
}

int main(void) {
    list_print(list_input(person_read), person_print);
}
