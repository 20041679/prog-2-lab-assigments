#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ARR_LEN(arr) (sizeof(arr) / sizeof(*arr))
#define FILE_END "END" 

typedef struct {
    char first_name[64];
    char last_name[64];
    unsigned int age;
} Person;

char *fgets_strip(char *s, int size, FILE *stream) {
    char *ret = fgets(s, size, stream);
    s[strcspn(s, "\n")] = 0;
    return ret;
}

int person_parse(char *s, Person *person) {
    char format[4 + 1 + 4 + 1 + 2 + 1];
    assert((size_t)snprintf(
        format,
        ARR_LEN(format),
        "%%%zus %%%zus %%u",
        ARR_LEN(person->first_name),
        ARR_LEN(person->last_name)
    ) < ARR_LEN(format));

    return sscanf(
        s,
        format,
        person->first_name,
        person->last_name,
        &person->age
    ) < 3 ? -1 : 0;
}

int person_read(FILE *f, Person *person) {
    char buf[256];
    fgets_strip(buf, ARR_LEN(buf), f);

    if (strncmp(buf, FILE_END, ARR_LEN(FILE_END)) == 0) {
        return 0;
    }
    return person_parse(buf, person) < 0 ? 0 : 1;
}

void person_print(Person *person) {
    printf(
        "nome: %s, cognome: %s, eta': %u\n",
        person->first_name,
        person->last_name,
        person->age
    );
}

int person_is_adult(Person *person) {
    return person->age >= 18;
}

typedef Person Data;

typedef struct ListHead {
    struct ListHead *next;
    Data data;
} ListHead;

typedef ListHead *Link;

Link list_new_raw(void) {
    return malloc(sizeof(ListHead));
}

Link list_new(Data data) {
    Link ret = list_new_raw();
    ret->data = data;
    ret->next = NULL;
    return ret;
}

Link list_input(FILE *f, int (*input) (FILE *, Data *)) {
    ListHead *head = NULL;
    ListHead **next = &head;

    Data data;
    while (input(f, &data)) {
        *next = list_new(data);
        next = &(*next)->next;
    }
    return head;
}

size_t list_print_filter(Link head, void (*print) (Data *), int (*filter) (Data *)) {
    size_t count = 0;
    while (head) {
        Data *data = &head->data;
        if (filter(data)) {
            print(data);
            ++count;
        }
        head = head->next;
    }
    return count;
}

int main(void) {
    FILE *f = fopen("input.txt", "r");
    Link list = list_input(f, person_read);
    fclose(f);
    size_t n_adults = list_print_filter(list, person_print, person_is_adult);
    printf("numero adulti: %zu\n", n_adults);
}
