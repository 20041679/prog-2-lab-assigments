#include "list.h"
#include <stdlib.h>

List node_alloc() {
    return malloc(sizeof(Head));
}

void list_input_insert_tail(List *list, int (*input) (Data *)) {
    if (!(*list)) {
        List next = *list;
        (*list) = node_alloc();
        Data data;
        input(&data);
        (*list)->data = data;
        (*list)->next = next;
    } else {
        list_input_insert_tail(&(*list)->next, input);
    }
}

void list_filter_output(
    List list,
    int (*predicate) (Data *, void *),
    void *key,
    void (*output) (Data *))
{
    if (list) {
        if (predicate(&list->data, key)) {
            output(&list->data);
        }
        list_filter_output(list->next, predicate, key, output);
    }
}

void list_rev_filter_output(
    List list,
    int (*predicate) (Data *, void *),
    void *key,
    void (*output) (Data *))
{
    if (list) {
        list_rev_filter_output(list->next, predicate, key, output);
        if (predicate(&list->data, key)) {
            output(&list->data);
        }
    }
}
