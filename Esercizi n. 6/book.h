#ifndef BOOK_H
#define BOOK_H

typedef struct {
    char title[64];
    char author[64];
    float price;
    unsigned int copies;
} Book;

int book_input(Book *book);
void book_output(Book *book);
int book_has_copies(Book *book, unsigned int n);

#endif
