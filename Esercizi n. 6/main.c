#include "list.h"

void f1(List *list) {
    return list_input_insert_tail(list, book_input);
}

int has_copies(Book *book, void *n) {
    return book_has_copies(book, *(int *)n);
}

void f2(List list, unsigned int x) {
    list_filter_output(list, has_copies, &x, book_output);
}

void f3(List list, unsigned int x) {
    list_rev_filter_output(list, has_copies, &x, book_output);
}

int main(void) { }
