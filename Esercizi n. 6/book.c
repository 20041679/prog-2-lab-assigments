#include "book.h"
#include <stdio.h>
#include <string.h>

int book_input(Book *book) {
    puts("titolo");
    scanf("%63s", book->title);
    puts("autore");
    scanf("%63s", book->author);
    puts("price");
    scanf("%f", &book->price);
    scanf("%u", &book->copies);
    return 1;
}

void book_output(Book *book) {
    printf(
        "titolo: %s, autore: %s, prezzo: %f, disponibilità: %u\n",
        book->title,
        book->author,
        book->price,
        book->copies
    );
}

int book_has_copies(Book *book, unsigned int n) {
    return book->copies >= n;
}

int book_cmp(Book *book1, Book *book2) {
    return
        strcmp(book1->title, book2->title) == 0 &&
        strcmp(book1->author, book2->author) == 0 &&
        book1->price == book2->price;
}
