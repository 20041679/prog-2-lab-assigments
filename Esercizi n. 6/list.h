#ifndef LIST_H
#define LIST_H
#include "book.h"

typedef Book Data;

typedef struct Head {
    struct Head *next;
    Data data;
} Head;

typedef Head *List;

List node_alloc();
void list_input_insert_tail(List *list, int (*input) (Data *));
void list_filter_output(List list, int (*predicate) (Data *, void *), void *key, void (*output) (Data *));
void list_rev_filter_output(List list, int (*predicate) (Data *, void *), void *key, void (*output) (Data *));

#endif
