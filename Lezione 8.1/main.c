#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef Data int;

typedef struct ListHead {
    struct ListHead *next;
    Data data;
} ListHead;

typedef ListHead *Link;

Link list_new_raw(void) {
    return malloc(sizeof(ListHead));
}

typedef enum { HEAD, TAIL } Mode;
typedef enum { ITER, RECUR } Process;

void usage() {
   fprintf(stderr, "%s", "<prog_name> <n> <h/t> <i/r>\n");
}

int main(int argc, char *argv[]) {
    if (argc < 4) {
        usage();
        return EXIT_FAILURE;
    }

    char *end;
    size_t n = strtol(argv[1], &end, 10);
    if (!end) {
        usage();
        return EXIT_FAILURE;
    }

    Mode mode;
    if (strcmp(argv[2], "h") == 0) {
        mode = HEAD;
    } else if (strcmp(argv[2], "t") == 0) {
        mode = TAIL;
    } else {
        usage();
        return EXIT_FAILURE;
    }

    Process process;
    if (strcmp(argv[3], "i") == 0) {
        process = ITER;
    } else if (strcmp(argv[3], "r") == 0) {
        process = RECUR;
    } else {
        usage();
        return EXIT_FAILURE;
    }

    srand(time(NULL));
}
