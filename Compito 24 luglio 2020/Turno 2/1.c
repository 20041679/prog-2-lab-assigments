Link f(Link list1, Link list2) {
    while (list1 && list2) {
        list1 = list1->next;
        list2 = list2->next;
    }

    Link ret;
    Link *tail = &ret;

    while (list1) {
        Link new = new_node();
        node->data = list1->data;
        list1 = list1->next;
        *tail = new;
        tail = &new->next;
    }

    while (list2) {
        Link new = new_node();
        node->data = list2->data;
        list2 = list2->next;
        *tail = new;
        tail = &new->next;
    }

    *tail = NULL;
    return ret;
}

// Complessità in tempo: O(max(|list1|, |list2|))
// Complessità in spazio: O(1)
