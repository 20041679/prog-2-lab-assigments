void f(Link *list, int k) {
    while (*list) {
        if ((*list)->data == k) {
            Link after = (*list)->next;
            free(*list);
            *list = after;
            break;
        }
        list = &(*list)->next;
    }
}

// Complessità in tempo: O(|list|)
// Complessità in spazio: O(1)
