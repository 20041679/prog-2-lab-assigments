void f(Link *list, size_t k) {
    Link *at;
    if ((at = find_pos(list, pos))) {
        Link after = (*at)->next;
        free(*at);
        *at = after;
    }
}

Link *find_pos(Link *list, size_t pos) {
    if (list) {
        return pos == 1
            ? list
            : find_pos(&(*list)->next);
    } else {
        return NULL;
    }
}

// Complessità in tempo: O(min(k, |list|))
// Complessità in spazio: O(min(k, |list|))
