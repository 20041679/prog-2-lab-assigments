size_t f(Link list) {
    size_t max_length = 0;
    size_t length = 0;
    int current;
    if (list) {
        current = list->data;
        ++length;
    }
    list = list->next;

    while (list) {
        if (list->data == current) {
            ++length;
        } else {
            current = list->data;
            if (length > max_lenght) {
                max_length = length;
            }
            length = 1;
        }
        list = list->next;
    }
    return max_length;
}

// Complessità in tempo: O(|list|)
// Complessità in spazio: O(1)
