#include <stdio.h>
#include <string.h>
#include <stdlib.h>

unsigned count_vowels(const char *s) {
    unsigned ret = 0;
    const char *vowels = "aeiouAEIOU";
    char *vocal_pos;
    while ((vocal_pos = strpbrk(s, vowels))) {
        s = vocal_pos + 1;
        ++ret;
    }
    return ret;
}

int main(void) {
    FILE *f_in;
    if (!(f_in = fopen("surnames.txt", "r"))) {
        perror("failed to open input file");
        return EXIT_FAILURE;
    }

    FILE *f_out;
    if (!(f_out = fopen("vowels-count.txt", "w"))) {
        perror("failed to open output file");
        return EXIT_FAILURE;
    }

    char buf[64];
    while (fgets(buf, sizeof(buf), f_in)) {
        fprintf(f_out, "Cognome: %s\nVocali: %u\n", buf, count_vowels(buf));
        fputs("*****\n", f_out);
    }

    fclose(f_in);
    fclose(f_out);
}
