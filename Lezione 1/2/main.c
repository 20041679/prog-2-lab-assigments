#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define FAMILY_NAME "rossi"

int main(void) {
    FILE *f_in;
    if (!(f_in = fopen("names.txt", "r"))) {
        perror("failed to open input file");
        return EXIT_FAILURE;
    }

    FILE *f_out;
    if (!(f_out = fopen("parenthood.txt", "w"))) {
        perror("failed to open output file");
        return EXIT_FAILURE;
    }

    char buf[64];
    while (fgets(buf, sizeof(buf), f_in)) {
        char first_name[64];
        char last_name[64];

        sscanf(buf, "%s %s\n", first_name, last_name);

        fprintf(f_out, "Nome: %s\nCognome: %s\nParente? %s\n",
            first_name,
            last_name,
            strcasecmp(last_name, FAMILY_NAME) == 0
                ? "SI"
                : "NO"
        );
        fputs("*****\n", f_out);
    }

    fclose(f_in);
    fclose(f_out);
}
