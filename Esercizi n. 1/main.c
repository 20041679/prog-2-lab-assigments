#include <stdio.h>
#include <stdlib.h>

typedef struct {
    char first_name[20];
    char last_name[25];
    unsigned int age;
} Person;

void person_input(Person *person) {
    puts("nome");
    scanf("%s", person->first_name);
    puts("cognome");
    // FIXME: buffer overflow
    scanf("%s", person->last_name);
    puts("anni");
    scanf("%u", &person->age);
}

void person_array_input(Person people[], size_t n) {
    for (size_t i = 0; i < n; ++i) {
        printf("persona %zu\n", i);
        person_input(&people[i]);
    }
}

void person_output(Person *person) {
    printf(
        "nome: %s, cognome: %s, anni: %u\n",
        person->first_name,
        person->last_name,
        person->age
    );
}

void person_array_idx_output(Person people[], size_t i) {
    person_output(&people[i]);
}

void person_array_output(Person people[], size_t n) {
    for (size_t i = 0; i < n; ++i) {
        person_array_idx_output(people, i);
    }
}

void split_adulthood(Person people[], size_t n, Person minors[], Person adults[]) {
    minors = malloc(sizeof(*people) * n);
    adults = malloc(sizeof(*people) * n);

    size_t count_minors = 0;
    size_t count_adults = 0;

    for (size_t i = 0; i < n; ++i) {
        Person *dst = people[i].age < 18
          ? &minors[count_minors++]
          : &adults[count_adults++];
        *dst = people[i];
    }

    minors = realloc(minors, sizeof(*people) * count_minors);
    adults = realloc(adults, sizeof(*people) * count_adults);
}
