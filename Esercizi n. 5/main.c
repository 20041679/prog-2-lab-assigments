#include <stdio.h>
#include "person.h"
#include "list.h"

int person_is_relative_list(List list, Person *person) {
    return person_is_relative(&list->data, person);
}

void search(List list) {
    puts("tipo di ricerca ([p]osizione/[c]ogonme)");
    char c;
    scanf(" %c", &c);

    List found;
    switch (c) {
        case 'p': {
            puts("inerire posizione");
            size_t pos;
            scanf("%zu", &pos);

            found = list_nth(list, pos);
            break;
        }
        case 'c': {
            puts("inserire cognome");
            Person key;
            scanf("%63s", key.last_name);

            found = list_find(list, person_is_relative_list, &key);
            break;
        }
        default:
            puts("input non valido");
            return;
    }
    if (found) {
        person_output(&found->data);
    } else {
        puts("non trovata");
    }
}

void insert_relative(List list, Person person) {
    if (list_insert_after(list, person, person_is_relative_list, &person)) {
        list_output(list, person_output);
    } else {
        puts("non ha parenti");
    }
}

int main(void) {
    puts("inerimento lista, END per terminare");
    List list = list_input(person_input);

    search(list);

    puts("inserire persona da inserire nella lista");
    Person person;
    person_input(&person);
    insert_relative(list, person);
}
