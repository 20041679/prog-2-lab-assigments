#include <stdlib.h>
#include "list.h"

List node_alloc(void) {
    return malloc(sizeof(Head));
}

List list_input(int (*input) (Data *)) {
    Data data;
    List new = NULL;
    if (input(&data)) {
        list_insert_tail(&new, data);
        new->next = list_input(input);
    }
    return new;
}

// spero intendesse questo per modificarla
void list_insert_tail(List *list, Data data) {
    if (*list) {
        list_insert_tail(&(*list)->next, data);
    } else {
      *list = node_alloc();
      (*list)->data = data;
      (*list)->next = NULL;
    }
}

void list_output(List list, void (*print) (Data *)) {
    if (list) {
        print(&list->data);
        list_output(list->next, print);
    }
}

List *list_find_ref(List *list, int (*predicate) (List, Data *), Data *key) {
    if (*list) {
        return predicate(*list, key)
            ? list
            : list_find_ref(&(*list)->next, predicate, key);
    }
    return NULL;
}

List list_find(List list, int (*predicate) (List, Data *), Data *key) {
    List *ret = list_find_ref(&list, predicate, key);
    return ret
        ? *ret
        : NULL;
}

int list_insert_after(List list, Data data, int (*predicate) (List, Data *), Data *key) {
    List found;
    if ((found = list_find(list, predicate, key))) {
        List next = found->next;
        found->next = node_alloc();
        found->next->data = data;
        found->next->next = next;
        return 1;
    }
    return 0;
}

List list_nth(List list, size_t n) {
    if (n == 0) {
        return list;
    } else {
        return list_nth(list->next, n - 1);
    }
}
