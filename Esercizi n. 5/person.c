#include "person.h"
#include <string.h>
#include <stdio.h>

int person_input(Person *person) {
    scanf("%63s", person->first_name);
    if (strcmp(person->first_name, "END") == 0) {
        return 0;
    }
    scanf(
        "%63s %u",
        person->last_name,
        &person->age
    );
    return 1;
}

int person_is_relative(Person *p1, Person *p2) {
    return strcmp(p1->last_name, p2->last_name) == 0;
}

void person_output(Person *person) {
    printf(
        "nome: %s, cognome: %s, anni: %u\n",
        person->first_name,
        person->last_name,
        person->age
    );
}

int person_is_minor(Person *person) {
    return person->age < 18;
}
