#ifndef LIST_H
#define LIST_H

#include "person.h"

typedef Person Data;

typedef struct Head {
    Person data;
    struct Head *next;
} Head;

typedef Head *List;

List node_alloc(void);
List list_input(int (*input) (Data *));
void list_insert_tail(List *list, Data data);
int list_insert_after(List list, Data data, int (*predicate) (List, Data *), Data *key);
List *list_find_ref(List *list, int (*predicate) (List, Data *), Data *key);
void list_output(List list, void (*print) (Data *));
List list_find(List list, int (*predicate) (List, Data *), Data *key);
List list_nth(List list, size_t n);

#endif
