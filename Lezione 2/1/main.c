#include <stdio.h>
#include <string.h>

#define ARR_LEN(a) (sizeof(a) / sizeof(*a))

typedef struct {
    char first_name[64];
    char last_name[64];
    unsigned short age;
} Person;

void strip(char *s) {
    s[strcspn(s, "\n")] = '\0';
}

char *str_input(char *s, size_t size) {
    char *ret = fgets(s, size, stdin);
    strip(ret);
    return ret;
}

Person person_read() {
    Person ret;
    puts("nome");
    str_input((char *)&ret.first_name, ARR_LEN(ret.first_name));
    puts("cognome");
    str_input((char *)&(ret.last_name), ARR_LEN(ret.last_name));
    puts("eta'");
    scanf("%hu", &ret.age);
    return ret;
}

void person_print(Person person) {
    printf(
        "nome: %s\ncognome: %s\neta': %hu\n",
        person.first_name,
        person.last_name,
        person.age
    );
}

int main(void) {
    person_print(person_read());
}
