#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define ARR_LEN(a) (sizeof(a) / sizeof(*a))

typedef struct {
    char first_name[64];
    char last_name[64];
    unsigned short age;
} Person;

Person person_read(FILE *f) {
    Person person;
    char format[4 + 1 + 4 + 1 + 3 + 1];
    sprintf(
        format, "%%%zus %%%zus %%hu",
        ARR_LEN(person.first_name),
        ARR_LEN(person.last_name)
    );
    fscanf(
        f, format,
        person.first_name,
        person.last_name,
        &person.age
    );
    return person;
}

void person_print(FILE *f, Person person) {
    fprintf(f, 
        "[Nome]: %s [Cognome]: %s [Età]: %hu\n",
        person.first_name,
        person.last_name,
        person.age
    );
}

int main(void) {
    FILE *f_in; 
    if (!(f_in = fopen("in.txt", "r"))) {
        return EXIT_FAILURE;
    }

    Person p1 = person_read(f_in);
    Person p2 = person_read(f_in);

    fclose(f_in);

    FILE *f_out;
    if (!(f_out = fopen("out.txt", "w"))) {
        return EXIT_FAILURE;
    }
    person_print(f_out, p2);
    person_print(f_out, p1);

    fclose(f_out);
}
