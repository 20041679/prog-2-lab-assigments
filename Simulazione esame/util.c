#include "util.h"
#include <stddef.h>
#include <stdio.h>
#include <string.h>

char *fgets_strip(char *s, size_t size, FILE *stream) {
	char *ret = fgets(s, size, stream);
	s[strcspn(s, "\n")] = '\0';
	return ret;
}
