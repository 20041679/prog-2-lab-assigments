#include "list.h"
#include <stdlib.h>

Link list_new_raw() {
	return malloc(sizeof(Element));
}

Link list_new(Data data[], size_t n) {
	if (n == 0) {
			return NULL;
	}
	Link new = list_new_raw();
	new->data = data[0];
	new->next = list_new(&data[1], n - 1);
	return new;
}

Link list_read(FILE *f, int (*read) (FILE *, Data *)) {
	Data data;
	Link head = NULL;
	Link *last  = &head;
	while (read(f, &data)) {
		*last = list_new(&data, 1);
		last = &(*last)->next;
	}
	return head;
}

void list_print(Link head, FILE *f, void (*print) (FILE *f, Data *)) {
	if (head) {
		print(f, &head->data);
		list_print(head->next, f, print);
	}
}

float list_float_fold(Link head, float init, float (*op) (float, Data *)) {
	if (head) {
		return list_float_fold(head->next, op(init, &head->data), op);
	}
	return init;
}

Link list_filter(Link head, int (*predicate) (Data *, void *), void *param) {
	if (head) {
		if (predicate(&head->data, param)) {
			Link new = list_new(&head->data, 1);
			new->next = list_filter(head->next, predicate, param);
			
			return new;
		}
		return list_filter(head->next, predicate, param);
	}
	return NULL;
}
