#ifndef LIST_H
#define LIST_H

#include "product.h"

typedef Product Data;

typedef struct Element {
	Data data;
	struct Element *next;
} Element;

typedef Element *Link;

Link list_new(Data data[], size_t n);
Link list_read(FILE *f, int (*read) (FILE *, Data *));
void list_print(Link head, FILE *f, void (*print) (FILE *f, Data *));
float list_float_fold(Link head, float init, float (*op) (float, Data *));
Link list_filter(Link head, int (*predicate) (Data *, void *), void *param);

#endif
