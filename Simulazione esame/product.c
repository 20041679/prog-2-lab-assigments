#include "product.h"
#include "util.h"
#include <string.h>

int product_parse(char *s, Product *product) {
	char format[2 + 4 + 4 + 2 + 2 + 1];
	snprintf(format, ARR_LEN(format), "%%d%%%zus%%%zus%%f%%u",
	    ARR_LEN(product->producer),
		ARR_LEN(product->name));
	sscanf(s, format,
		&product->code,
		product->producer,
		product->name,
		&product->price,
		&product->quantity);
	if (product->code < 0 && strcmp(product->producer, END) == 0) {
		return 0;
	}
	return 1;
}

int product_read(FILE *f, Product *product) {
	char buf[256];
	fgets_strip(buf, ARR_LEN(buf), f);
	return product_parse(buf, product);
}

void product_print(FILE *f, Product *product) {
	fprintf(
		f,
		"Codice prodotto: %d\nNome prodotto: %s\nProdotto da: %s\nQuantità: %u\nPrezzo: %.2f\n",
		product->code,
		product->name,
		product->producer,
		product->quantity,
		product->price);
}

float product_price_total(Product *product) {
	return product->price * product->quantity;
}

int product_compare_producer(Product *product, char *s) {
	return strcmp(product->producer, s) == 0;
}
