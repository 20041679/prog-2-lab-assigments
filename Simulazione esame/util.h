#ifndef UTIL_H
#define UTIL_H

#include <stdio.h>

#define ARR_LEN(x) (sizeof(x) / sizeof(*x))

char *fgets_strip(char *s, size_t size, FILE *stream);

#endif
