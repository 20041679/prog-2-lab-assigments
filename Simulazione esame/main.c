#include "list.h"
#include "product.h"
#include "util.h"
#include <stdlib.h>

Link crealista(FILE *f) {
	return list_read(f, product_read);
}

void stampalista(Link head) {
	FILE *f;
	if ((f = fopen("output.txt", "a"))) {
		list_print(head, f, product_print);
		fclose(f);
	}
}

float sum(float acc, Product *p) {
	return acc + product_price_total(p);
}

float calcolaval(Link L1) {
	return list_float_fold(L1, 0, sum);
}

int compare_producer(Product *p, void *string) {
	return product_compare_producer(p, (char *)string);
}

Link listaforni(Link L1, char *fornitore) {
	return list_filter(L1, compare_producer, fornitore);
}

int main(void) {
	FILE *f;
	if (!(f = fopen("input.txt", "r"))) {
		return EXIT_FAILURE;
	}
	
	Link list = crealista(f);
	fclose(f);

	stampalista(list);

	printf("prezzo complessivo dei prodotti: %.2f\n", calcolaval(list));

	puts("inserire fornitore da filtrare:");
	char buf[64];
	fgets_strip(buf, ARR_LEN(buf), stdin);

	stampalista(listaforni(list, buf));
}
