#ifndef PRODUCT_H
#define PRODUCT_H

#define END "FINE_LISTA"

#include <stdio.h>

typedef struct {
	int code;
	char producer[64];
	char name[64];
	float price;
	unsigned quantity;
} Product;

int product_parse(char *s, Product *product);
int product_read(FILE *f, Product *product);
void product_print(FILE *f, Product *product);
float product_price_total(Product *product);
int product_compare_producer(Product *product, char *s);

#endif
