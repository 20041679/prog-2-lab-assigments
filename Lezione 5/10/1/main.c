#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ARR_LEN(arr) (sizeof(arr) / sizeof(*arr))
#define FILE_END "END" 

typedef struct {
    char first_name[64];
    char last_name[64];
    unsigned int age;
} Person;

char *fgets_strip(char *s, int size, FILE *stream) {
    char *ret = fgets(s, size, stream);
    s[strcspn(s, "\n")] = 0;
    return ret;
}

int person_parse(char *s, Person *person) {
    char format[4 + 1 + 4 + 1 + 2 + 1];
    assert((size_t)snprintf(
        format,
        ARR_LEN(format),
        "%%%zus %%%zus %%u",
        ARR_LEN(person->first_name),
        ARR_LEN(person->last_name)
    ) < ARR_LEN(format));

    return sscanf(
        s,
        format,
        person->first_name,
        person->last_name,
        &person->age
    ) < 3 ? -1 : 0;
}

int person_read(FILE *f, Person *person) {
    char buf[256];
    fgets_strip(buf, ARR_LEN(buf), f);

    if (strncmp(buf, FILE_END, ARR_LEN(FILE_END)) == 0) {
        return 0;
    }
    return person_parse(buf, person) < 0 ? 0 : 1;
}

void person_print(Person *person) {
    printf(
        "nome: %s, cognome: %s, eta': %u\n",
        person->first_name,
        person->last_name,
        person->age
    );
}

int person_is_relative(Person *p1, Person *p2) {
    return strcmp(p1->last_name, p2->last_name) == 0;
}

typedef Person Data;

typedef struct ListHead {
    struct ListHead *next;
    Data data;
} ListHead;

typedef ListHead *Link;

Link list_new_raw(void) {
    return malloc(sizeof(ListHead));
}

Link list_new(Data data) {
    Link ret = list_new_raw();
    ret->data = data;
    ret->next = NULL;
    return ret;
}

Link list_input(FILE *f, int (*input) (FILE *, Data *)) {
    ListHead *head = NULL;
    ListHead **next = &head;

    Data data;
    while (input(f, &data)) {
        *next = list_new(data);
        next = &(*next)->next;
    }
    return head;
}

Link list_find(Link head, Data *key, int (*compare) (Data *, Data *)) {
    while (head && !compare(&head->data, key)) {
        head = head->next;
    }
    return head;
}

void list_insert_tail(Link *head, Data data) {
    while (*head) {
        head = &(*head)->next;
    }
    Link tail = list_new(data);
    *head = tail;
}

void list_insert_head(Link *head, Data data) {
    Link new_head = list_new(data);
    new_head->next = *head;
    *head = new_head;
}

void list_print(Link head, void (*print) (Data *)) {
    while (head) {
        print(&head->data);
        head = head->next;
    }
}

int main(void) {
    FILE *f = fopen("input.txt", "r");
    Link list = list_input(f, person_read);
    fclose(f);

    Person to_insert;
    puts("inserire persona nel formato <nome> <cognome> <età>");
    while (!person_read(stdin, &to_insert)) {
        puts("formato non valido");
    }

    Link found;
    if ((found = list_find(list, &to_insert, person_is_relative))) {
        // can start at found instead of list, what's beofore
        // would be skipped anyways
        list_insert_tail(&found, to_insert);
    } else  {
        list_insert_head(&list, to_insert);
    }

    list_print(list, person_print);
}
