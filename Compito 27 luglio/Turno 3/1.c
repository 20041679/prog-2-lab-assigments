// Dalla consegna non si capisce niente, a partire dal fatto che
// si richiede n < m e nell'esempio n è 2 e m è 5.
// Inoltre cosa bisogna fare se i nodi non esistono tutti?
// E da che numero partono le posizioni?
// Gli estremi sono inclusi o esclusi?
// 
// Questa implementazione richiede n <= m (si può eliminare
// anche un singolo elemento) e elimina i nodi se e solo se
// esistono tutti, le posizioni partono da 0 e gli estremi
// sono inclusi
void f(Link *list, size_t n, size_t m) {
    size_t pos = 0;
    // skip the first `n` nodes
    while (*list && pos < n) {
        list = &(*list)->next;
        ++pos;
    }
    if (*list) {
        Link skip = *list;
        pos = 0;
        // traverse every node of the deletion area.
        while (skip && pos < m - n) {
            skip = skip->next;
            ++pos;
        }
        if (skip) {
            // every node that needs to be deleted exists,
            // proceed with deletion
            skip = *list;
            pos = 0;
            while (skip && pos < m - n) {
                Link next = skip->next;
                free(skip);
                skip = next;
                ++pos
            }
            // skip->next can be assumed to exist since this is the
            // true branch of `if (skip)`;
            // link the last node of the skipped area with the first
            // node after the deleted area
            *list = skip->next;
        }
    }
}
