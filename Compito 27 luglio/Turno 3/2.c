// first call: sum1 = 0, sum2 = 0
int f(Link list1, Link list2, int sum1, int sum2) {
    if (list1 && list2) {
        sum1 += list1->data;
        sum2 += list2->data;
        list1 = list1->next;
        list2 = list2->next;
    } else if (list1) {
        sum1 += list1->data;
        list1 = list1->next;
    } else if (list2) {
        sum2 += list2->data;
        list2 = list2->next;
    } else {
        return sum1 > sum2
            ? sum1
            : sum2;
    }
    return f(list1, list2, sum1, sum2);
}

// Complessità in tempo: O(max(|list1|, |list2|))
// Complessità in spazio: O(max(|list1|, |list2|))
