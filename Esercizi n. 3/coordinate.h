#ifndef COORDINATE_H
#define COORDINATE_H

#include <stdio.h>

typedef struct Coordinate {
    int x;
    int y;
} Coordinate;

int coordinate_compare(Coordinate *c1, Coordinate *c2);
int coordinate_first_quadrant(Coordinate *c, Coordinate *_);
int coordinate_input(Coordinate *c);
void coordinate_output(FILE *f, Coordinate *c);

#endif
