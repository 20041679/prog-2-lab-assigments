#ifndef LIST_H
#define LIST_H

#include "coordinate.h"
#include <stddef.h>
#include <stdio.h>

typedef Coordinate Data;

typedef struct Head {
    Data data;
    struct Head *next;
} Head;

typedef Head *List;

List list_new(Data data[], size_t n);
void append(List *list1, List list2);
List list_input_insert_head(int (*input) (Data *), size_t n);
List list_input_insert_head(int (*input) (Data *), size_t n);
List list_filter(List list, int (*predicate) (Data*, Data *), Data *key);
size_t list_length(List list);
void list_output(FILE *f, List list, void (*output) (FILE *, Data *));

#endif
