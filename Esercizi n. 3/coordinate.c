#include "coordinate.h"
#include <stdio.h>

int coordinate_compare(Coordinate *c1, Coordinate *c2) {
    return c1->x == c2->x && c1->y == c2->y;
}

int coordinate_first_quadrant(Coordinate *c, Coordinate *_) {
    (void)_;
    return c->x > 0 && c->y > 0;
}

int coordinate_input(Coordinate *c) {
    scanf("%d %d", &c->x, &c->y);
    return 1;
}

void coordinate_output(FILE *f, Coordinate *c) {
    fprintf(f, "%d %d\n", c->x, c->y);
}
