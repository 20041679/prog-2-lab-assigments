#include <stdio.h>
#include "list.h"
#include "coordinate.h"

int main(void) {
    puts("n");
    size_t n;
    scanf("%zu", &n);
    List list = list_input_insert_head(coordinate_input, n);

    puts("coordinata da ricercare");
    int x, y;
    scanf("%d %d", &x, &y);
    Coordinate key = {x, y};
    printf(
        "la coordinata compare %zu volte\n",
        list_length(list_filter(list, coordinate_compare, &key))
    );

    List filtered = list_filter(list, coordinate_first_quadrant, NULL);
    puts("coordinate nel primo quadrante");
    list_output(stdout, filtered, coordinate_output);

    puts("nome file");
    char buf[64];
    scanf("%63s", buf);
    FILE *f;
    if (!(f = fopen(buf, "w"))) {
        return 1;
    }
    list_output(f, list, coordinate_output);
    fclose(f);
}
