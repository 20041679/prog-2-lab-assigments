#include "list.h"
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
    
List _head_alloc() {
    return malloc(sizeof(Head));
}

List list_new(Data data[], size_t n) {
    if (n > 0) {
        List new = _head_alloc();
        new->data = data[0];
        new->next = list_new(&data[1], n - 1);
        return new;
    } else {
        return NULL;
    }
}

void append(List *list1, List list2) {
    while (*list1) {
        list1 = &(*list1)->next;
    }
    *list1 = list2;
}

void list_insert_head(List *list, Data data) {
    List prepend = _head_alloc();
    prepend->data = data;
    prepend->next = *list;
    *list = prepend;
}

List list_input_insert_head(int (*input) (Data *), size_t n) {
    List list = NULL;
    for (size_t i = 0; i < n; ++i) {
        Data data;
        if (input(&data)) {
            list_insert_head(&list, data);
        } else {
            break;
        }
    }
    return list;
}

List list_filter(List list, int (*predicate) (Data *, Data *), Data *key) {
    if (list) {
        if (predicate(&list->data, key)) {
            List ret = _head_alloc();
            ret->data = list->data;
            ret->next = list_filter(list->next, predicate, key);
            return ret;
        }
        return list_filter(list->next, predicate, key);
    }
    return NULL;
}

size_t list_length(List list) {
    return list
        ? 1 + list_length(list->next)
        : 0;
 }

void list_output(FILE *f, List list, void (*output) (FILE *, Data *)) {
    if (list) {
        output(f, &list->data);
        list_output(f, list->next, output);
    }
}
