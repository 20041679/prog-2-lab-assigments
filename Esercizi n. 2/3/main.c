#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    char first_name[12];
    char last_name[10];
    int age;
} Person;

typedef Person Data;

typedef struct Node {
    Data data;
    struct Node *next;
} Node;

typedef Node *List;

List _node_alloc(void) {
    return malloc(sizeof(Node));
}

List list_new(Data data[], size_t n) {
    List new;
    if (n == 0) {
        new = NULL;
    } else {
        new = _node_alloc();
        new->data = data[0];
        new->next = list_new(&data[1], n - 1);
    }
    return new;
}

List list_input(FILE *f) {
    Person data;

    // FIXME: buffer overflow
    int ret = fscanf(f, "%s %s %d", data.first_name, data.last_name, &data.age);
    if (ret < 3 || ret == EOF) {
        return NULL;
    }
    
    List new = list_new(&data, 1);
    new->data = data;
    new->next = list_input(f);

    return new;
}

void list_print(List list, size_t n) {
    while (list && n > 0) {
        printf(
            "nome: %s, cognome: %s, anni: %d\n",
            list->data.first_name,
            list->data.last_name,
            list->data.age
        );
        list = list->next;
        --n;
    }
}

int main(void) {
    FILE *f = fopen("input.txt", "r");
    if (!f) { return 1; }
    list_print(list_input(f), 5);
    fclose(f);
}
