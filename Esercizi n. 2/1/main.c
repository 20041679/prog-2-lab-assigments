#include <stdio.h>
#include <stdlib.h>

typedef int Data;

typedef struct Node {
    Data data;
    struct Node *next;
} Node;

typedef Node *List;

List _node_alloc(void) {
    return malloc(sizeof(Node));
}

List list_new(Data data[], size_t n) {
    List new;
    if (n == 0) {
        new = NULL;
    } else {
        new = _node_alloc();
        new->data = data[0];
        new->next = list_new(&data[1], n - 1);
    }
    return new;
}

List list_input(void) {
    // stop at -1?
    int data;
    puts("inserire dato");
    scanf("%d", &data);

    List new;
    if (data == -1) {
        new = NULL;
    } else {
        new = list_new(&data, 1);
        new->data = data;
        new->next = list_input();
    }
    return new;
}

void list_print(List list) {
    while (list) {
        printf("%d ", list->data);
        list = list->next;
    }
    puts("");
}

int main(void) {
    list_print(list_input());
}
