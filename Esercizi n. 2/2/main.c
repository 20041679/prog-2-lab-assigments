#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    char first_name[12];
    char last_name[10];
    int age;
} Person;

typedef Person Data;

typedef struct Node {
    Data data;
    struct Node *next;
} Node;

typedef Node *List;

List _node_alloc(void) {
    return malloc(sizeof(Node));
}

List list_new(Data data[], size_t n) {
    List new;
    if (n == 0) {
        new = NULL;
    } else {
        new = _node_alloc();
        new->data = data[0];
        new->next = list_new(&data[1], n - 1);
    }
    return new;
}

List list_input(void) {
    Person data;

    puts("nome");
    // FIXME: buffer overflow
    scanf("%s", data.first_name);
    if (strcmp(data.first_name, "stop") == 0) {
        return NULL;
    }

    puts("cognome");
    // FIXME: buffer overflow
    scanf("%s", data.last_name);

    puts("anni");
    scanf("%d", &data.age);

    List new;
    
    new = list_new(&data, 1);
    new->data = data;
    new->next = list_input();

    return new;
}

void list_print(List list, size_t n) {
    while (list && n > 0) {
        printf(
            "nome: %s, cognome: %s, anni: %d\n",
            list->data.first_name,
            list->data.last_name,
            list->data.age
        );
        list = list->next;
        --n;
    }
}

int main(void) {
    list_print(list_input(), 5);
}
